

<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4: Access Modifiers and Encapsulation</title>
</head>
<body>

	<h2>Building</h2>

	<p><?= $building->printName(); ?></p>

	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p>
	
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>

	<p><?= $building->setName("Caswynn Complex"); ?></p>

	<p>The name of the building has been changed to <?= $building->getName(); ?></p>

	<h2>Condominium</h2>

	<p><?= $condominium->printName(); ?></p>

	<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
	
	<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>

	<p><?= $condominium->setName("Enzo Tower"); ?></p>

	<p>The name of the condominium has been changed to <?= $condominium->getName(); ?></p>


	
	

	



</body>
</html>