

<?php

// the keyword "public" is an example of an access modifier

class Building {
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		 $this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floor){
		 $this->floors = $floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setAddress($address){
		 $this->address = $address;
	}

	//Class methods
	public function printName(){
		return "The name of the building is $this->name";
	}
}

//[SECTION] Inheritance and Polymorphism

class Condominium extends Building {

	public function printName(){
		return "The name of the condominium is $this->name";
	}

}

//Object creation from class
$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');